Install PostgreSQL 9.3 on Ubuntu - May 2014
-------------------------------------------

Content:

* Installs PostgreSQL 9.3 on Ubuntu 14.04 LTS (Trusty Tahr).
* Installs Supervisor.

Note: nothing is configured.

(this is the base image for `tinyerp/ubuntu-openerp-*` builds)
