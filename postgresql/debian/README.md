Install PostgreSQL 9.3 on Debian - May 2014
-------------------------------------------

Content:

* Installs PostgreSQL 9.3 on Debian Jessie.
* Installs Supervisor.

Note: nothing is configured.

(this is the base image for `tinyerp/debian-openerp-*` builds)
